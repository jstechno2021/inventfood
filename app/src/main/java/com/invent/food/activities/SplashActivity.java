package com.invent.food.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.Window;

import com.invent.food.R;
import com.invent.food.Utils.Constant;
import com.invent.food.Utils.Utility;

public class SplashActivity extends Activity {

    private Context mContext;
    private final int SPLASH_DISPLAY_LENGTH = 2000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_splash);
        mContext = this;

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if (Utility.getAppPrefString(mContext, Constant.isLogin).equalsIgnoreCase("true")) {
                    Intent intent = new Intent(mContext, RegisterActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
                } else {
                    Intent intent = new Intent(mContext, RegisterActivity.class);
                    startActivity(intent);
                    finish();
                    overridePendingTransition(R.anim.fade_in, R.anim.fade_out);
//                    Intent intent = new Intent(mContext, LoginActivity.class);
//                    startActivity(intent);
//                    finish();
                }
            }
        }, SPLASH_DISPLAY_LENGTH);

    }
}
