package com.invent.food.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import com.invent.food.R;

public class RegisterActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
    }

    public void onBack(View view){
        finish();
    }

    public void onLogin(View view){
        Intent intent = new Intent(RegisterActivity.this, LoginActivity.class);
        startActivity(intent);
    }

    public void onRegister(View view){
//        Intent intent = new Intent(RegisterActivity.this, MainActivity.class);
        Intent intent = new Intent(RegisterActivity.this, MyOrderActivity.class);
        startActivity(intent);
    }
}