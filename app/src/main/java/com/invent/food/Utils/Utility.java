package com.invent.food.Utils;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.util.DisplayMetrics;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

@SuppressLint("SimpleDateFormat")
public class Utility {

    private static final String TAG = Utility.class.getSimpleName();
    private Context context;

    public Utility(Context context) {
        this.context = context;
    }

    public static void toast(String toastMessage, Context context) {
        Toast.makeText(context, toastMessage, Toast.LENGTH_LONG).show();
    }

    public static float convertPixelsToDp(float px, Context context) {
        return px / ((float) context.getResources().getDisplayMetrics()
                .densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    public static void writeSharedPreferences(@NonNull Context mContext, String key, String value) {
        try {
            SharedPreferences settings = mContext.getSharedPreferences(
                    Constant.PREFS_NAME, 0);
            SharedPreferences.Editor editor = settings.edit();
            editor.putString(key, value);
            editor.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Nullable
    public static String getAppPrefString(@NonNull Context mContext, String key) {
        try {
            SharedPreferences settings = mContext.getSharedPreferences(
                    Constant.PREFS_NAME, 0);
            String value = settings.getString(key, "");
            return value;
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

}
