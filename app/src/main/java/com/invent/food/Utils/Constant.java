package com.invent.food.Utils;

import androidx.annotation.NonNull;

public class Constant {

    @NonNull
    public static String PREFS_NAME = "InvetPref";
    @NonNull
    public static String TAG = "Invet";
    public static int MY_SOCKET_TIMEOUT_MS = 60000;

//    @NonNull
//    public static String URL_PARENT = "http://15.207.68.109/emmdemo/rest/V1/customer/"; // Testing


    @NonNull
    public static String isLogin = "is_login";
}
